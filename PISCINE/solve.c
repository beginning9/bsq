#include "solve.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

Square first_square(map* my_map){
	Square res = my_map->best_squares[0];
	int best_height = res.p.x;
	int best_left = res.p.y;
	for (int i = 0; i< my_map->taille_logique_sq; i++){
		Square current_square = my_map->best_squares[i];
		if (current_square.p.x < best_height){
			res = current_square;
		}
		else if ((current_square.p.x == best_height)&& (current_square.p.y < best_left)){
			res = current_square;
		}
	}
	return res;
}

Square Solve(map* my_map){
	my_map->best_size = 1;
	for (int i= 0; i< my_map->nbr_lignes;i++){
		for (int j= 0; j < my_map->nbr_colonnes; j++){
			Point current_point;
			current_point.y = i;
			current_point.x = j;
			int size = 1;
			int y = i +1;
			int x = j +1;
			while ((x<my_map->nbr_colonnes)&&(y<my_map->nbr_lignes)){
				if (! check_square(my_map,current_point,size)){
					break;
				}
				size++;
				x++;
				y++;
			}
			
			Square current_square;
			current_square.p = current_point;
			current_square.size = size;
			if (size>my_map->best_size){
				reset_best_squares(my_map);
				add_square(my_map,current_square);
				my_map->best_size=size;
			}
			else if (size == my_map->best_size){
				add_square(my_map,current_square);
			}


		}
	}
	Square res = first_square(my_map);
	return res;
}

