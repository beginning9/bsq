#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include "solve.h"
#include "tools.h"

bool check_map(char* buffer,int start,char obs,char full,char vide,int nbr_col,int nbr_ligne){

  bool error = false;
  bool is_map = false;
  bool check_obs= false;
  int i = 0;
  int j = 0;
  int index = start+1;

  while(buffer[index]){
    
    if (buffer[index]=='\n'){
      if (j!=nbr_col){

        error = true;
        }
      i +=1;
      j=0;
      if (i != nbr_ligne){
        if (buffer[index+nbr_col+1] != '\n'){
          error = true;
        }
      }
    }
    if (buffer[index]== obs){
        check_obs = true;
    }
    if ( buffer[index] != '\n'){
      j++;
    }

    if ((buffer[index] !=obs)&&(buffer[index]!=full)&&(buffer[index]!=vide)&&(buffer[index]!='\n')){
      error = true;
    }
    if (error){
      perror("map error\n");
      exit(1);
    }
    index++;
  }

  if ((!error)&&(check_obs)){
    is_map = true;
  }
  return is_map;
}


void dessine(char* buffer,Square solved,int start,char char_full){
  int i = 0;
  int j = 0;
  int index = start +1;
  while(buffer[index]){
    if (buffer[index]=='\n'){
      i +=1;
      j=0;
      
    }
    if (( (solved.p.x <= j)&& (j < solved.p.x + solved.size))
      && ( (solved.p.y <= i)&& (i < solved.p.y + solved.size))){
        buffer[index]= char_full; 
    }
    if ( buffer[index] != '\n'){
      j++;
    }
    index++;
    
  }

}

void affiche_map(char* buffer,int start){
  int index = start +1;
  while(buffer[index]){
    printf("%c",buffer[index]);
    index++;
  }

}
int get_param(char * buffer){
  int i =0;
  bool endl = false;
  int nbr_param=0;
  while( buffer[i]){

    if (buffer[i]=='\n') {
      endl =true;
      break;}   
    i++;
    nbr_param++;
  }
  if (!endl){

    perror("map error\n");
    exit(1);
  }
  else if(nbr_param <4){
    perror("map error\n");
    exit(1);
  }
  return nbr_param;
}

int get_width(char *buffer){
  int i = 0;
  int start=0;
  int nbr_col = 0;
  while (buffer[i])
    {
      if (start != 0  && buffer[i]!= '\n'){
        nbr_col++;
      } 
      if (start != 0 && buffer[i]== '\n'){
          break;
      }
      if (buffer[i] == '\n'){
          start = i;
      }
      i++;
    }
  return nbr_col;
  }

void get_obstacle(map* my_map,char* buffer, int start,char obs){
  int i = 0;
  int j = 0;
  int index = start +1;
 while(buffer[index]){
    if (buffer[index]=='\n'){
      i +=1;
      j=0;
    }
    if (buffer[index] == obs){
      Point obstacle;
      obstacle.x = j-1;
      obstacle.y = i;
      add_point(my_map,obstacle);
    }
    index++;
    j++;
  }
}

int main(int argc, char const *argv[]) {
  
  int fd;
  int size;
  char *buffer;
  const char* path = argv[1];

  map my_map;
  buffer = malloc(sizeof(char) * 1024);
  fd = open(path, O_RDONLY);
  size = read(fd, buffer, sizeof(char) * 1024);
  int i = 0;
  int j = 0;
  int nbr_ligne = 0;
  int nbr_col = get_width(buffer);
  char char_full;
  char char_obs;
  char char_vide;
  int start = get_param(buffer);
  
  while (buffer[i] ){

    i++;
    if (buffer[i]=='\n'){
      nbr_ligne++;
    }
    if ( i == start-3){
        char_vide= buffer[i];
    }
    if ( i == (start-2)){
        char_obs= buffer[i];
    }
    if ( i == (start-1)){
        char_full= buffer[i];
      }
  }
  my_map.nbr_colonnes = nbr_col;
  my_map.nbr_lignes = nbr_ligne-1;
  nbr_ligne -=1;

  if (!check_map(buffer,start,char_obs,char_full,char_vide,nbr_col,nbr_ligne)){
    perror("map error\n");
    exit(1);
  }
  liste_init(&my_map);
  get_obstacle(&my_map,buffer, start,char_obs);
  Square res = Solve(&my_map);
  dessine(buffer,res,start,char_full);
  printf("\nvoici la solution :\n");
  affiche_map(buffer,start);
  /*
  FILE* f = fopen("result.txt", "w");
  fwrite(buffer, sizeof(char), strlen(buffer), f);
*/

}