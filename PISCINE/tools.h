#ifndef __TOOLS_H
#define __TOOLS_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>

typedef struct {
    int x;
    int y;
} Point;

typedef struct{
	Point p;
	int size;
} Square;

typedef struct {
    Point* obstacles;
    int taille_logique;
    int taille_physique;

    Square* best_squares;
    int taille_logique_sq;
    int taille_physique_sq;
    
    int nbr_colonnes;
    int nbr_lignes;
    int best_size;

} map;


bool liste_init(map* liste);

void extend_point(map* liste);

void add_point(map* liste, Point element);

void reset_best_squares(map* liste);

void extend_squares(map* liste);

void add_square(map* liste, Square element);

bool check_square(map* my_map, Point other, int size_of_square);

#endif