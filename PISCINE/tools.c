#include "tools.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>


bool liste_init(map* liste) {
  liste->taille_logique = 0;
  liste->taille_physique = 20;
  liste->taille_logique_sq = 0;
  liste->taille_physique_sq = 100;
  liste->obstacles = (Point*)malloc(liste->taille_physique * sizeof(Point));
  liste->best_squares = (Square*)malloc(liste->taille_physique * sizeof(Square));
  if (liste->obstacles == NULL) {
    return false;
  }
  return true;
}

void extend_point(map* liste) {
  if (liste->taille_logique >= liste->taille_physique) {
    Point* anciennes_obstacles = liste->obstacles;
    size_t ancienne_taille_physique = liste->taille_physique;
    liste->taille_physique = liste->taille_physique * 2;
    liste->obstacles = (Point*)malloc(liste->taille_physique * sizeof(Point));
    memcpy(liste->obstacles, anciennes_obstacles, ancienne_taille_physique * sizeof(Point));
    free(anciennes_obstacles);
  }
}

void add_point(map* liste, Point element) {
  extend_point(liste);
  liste->obstacles[liste->taille_logique] = element;
  liste->taille_logique++;
}

void reset_best_squares(map* liste){
	Square* anciennes_best_squares = liste->best_squares;
  liste->best_squares = (Square*)malloc(liste->taille_physique_sq * sizeof(Square));
  free(anciennes_best_squares);
  liste->taille_logique_sq =0;
}
void extend_squares(map* liste) {
  if (liste->taille_logique_sq >= liste->taille_physique_sq) {
    Square* anciennes_best_squares = liste->best_squares;
    size_t ancienne_taille_physique = liste->taille_physique_sq;
    liste->taille_physique = liste->taille_physique_sq * 2;
    liste->best_squares = (Square*)malloc(liste->taille_physique_sq * sizeof(Square));
    memcpy(liste->obstacles, anciennes_best_squares, ancienne_taille_physique * sizeof(Square));
    free(anciennes_best_squares);
  }
}

void add_square(map* liste, Square element) {

  extend_squares(liste);
  liste->best_squares[liste->taille_logique_sq] = element;
  liste->taille_logique_sq++;
}

bool check_square(map* my_map, Point other, int size_of_square){
	for (int i = 0; i < my_map->taille_logique; i++){
		Point current_obstacle = my_map->obstacles[i];
		if (( (other.x <= current_obstacle.x)&& (current_obstacle.x <= other.x + size_of_square))
		 	&& ( (other.y <= current_obstacle.y)&& (current_obstacle.y <= other.y + size_of_square))){
			return false;
		}
	}
	return true;
}